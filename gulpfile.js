var gulp = require('gulp');
var sass = require('gulp-sass');
var watch = require('gulp-watch');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var connect = require('gulp-connect');

var paths = {
  sass: ['./scss/app.scss'],
  public: ['./public/**/*']
};

gulp.task('default', ['sass', 'watch', 'connect']);

gulp.task('sass', function (done) {
  gulp.src(paths.sass)
    .pipe(sass())
    .pipe(gulp.dest('./public/css/'))
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({
      extname: '.min.css'
    }))
    .pipe(gulp.dest('./public/css/'))
    .on('end', done);
});

gulp.task('watch', function () {
  gulp.watch(paths.sass, ['sass']);
  gulp.watch(paths.public, ['reload']);
});

gulp.task('reload', function () {
  gulp.src(paths.public)
    .pipe(connect.reload());
});

gulp.task('connect', function () {
  connect.server({
    root: 'public',
    port: 8000,
    host: '0.0.0.0',
    livereload: true
  });
});
