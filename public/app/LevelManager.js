/*global Hadrons*/
(function() {
  'use strict';
  var self = {},
    levels = [],
    currentLevel = 0,
    removeCurrentLevel,
    addNextLevel,
    createBlocksColumn,
    createBlocksRow,
    getGround;

  getGround = function() {
    return {
      x: 0,
      y: 550,
      name: 'ground',
      group: 'platforms',
      immovable: true
    };
  };

  createBlocksRow = function(row, column, length) {
    var blocks = [],
      i;


    for (i = 0; i < length; i += 1) {
      blocks.push({
        x: (column * 30) + (30 * i),
        y: row * 30,
        name: 'block',
        group: 'platforms',
        immovable: true
      });
    }

    return blocks;
  };

  createBlocksColumn = function(row, column, length) {
    var blocks = [],
      i;


    for (i = 0; i < length; i += 1) {
      blocks.push({
        x: column * 30,
        y: (row * 30) + (30 * i),
        name: 'block',
        group: 'platforms',
        immovable: true
      });
    }

    return blocks;
  };


  levels.push({
    rawObjects: [].concat(
      createBlocksColumn(5, 0, 15),
      getGround(), {
        x: 250,
        y: 200,
        name: 'beer',
        group: 'beer'
      }, {
        x: 350,
        y: 205,
        name: 'cerveja',
        group: 'beer'
      }
    ),
    rawGroups: [{
      name: 'platforms',
      enableBody: true
    }, {
      name: 'beer',
      enableBody: false
    }]
  });

  levels.push({
    rawObjects: [].concat(
      createBlocksRow(15, 10, 20),
      getGround()
    ),
    rawGroups: [{
      name: 'platforms',
      enableBody: true
    }]
  });
  levels.push({
    rawObjects: [].concat(
      createBlocksRow(15, 0, 20),
      getGround()
    ),
    rawGroups: [{
      name: 'platforms',
      enableBody: true
    }]
  });
  levels.push({
    rawObjects: [].concat(
      getGround(), {
        x: 250,
        y: 200,
        name: 'beer',
        group: 'beer'
      }, {
        x: 350,
        y: 205,
        name: 'cerveja',
        group: 'beer'
      }
    ),
    rawGroups: [{
      name: 'platforms',
      enableBody: true
    }, {
      name: 'beer',
      enableBody: true
    }]
  });
  levels.push({
    rawObjects: [].concat(
      createBlocksRow(15, 10, 2),
      createBlocksRow(10, 15, 2),
      createBlocksRow(5, 20, 2),
      getGround()
    ),
    rawGroups: [{
      name: 'platforms',
      enableBody: true
    }]
  });
  levels.push({
    rawObjects: [].concat(
      createBlocksRow(5, 2, 2),
      createBlocksColumn(10, 10, 10),
      getGround()
    ),
    rawGroups: [{
      name: 'platforms',
      enableBody: true
    }]
  });
  levels.push({
    rawObjects: [].concat(
      createBlocksColumn(10, 0, 10),
      createBlocksColumn(10, 25, 10),
      getGround(), {
        x: 280,
        y: 255,
        name: 'aninha',
        group: 'beer'
      }, {
        x: 180,
        y: 0,
        name: 'surpresa',
        group: 'beer'
      }
    ),
    rawGroups: [{
      name: 'platforms',
      enableBody: true
    }, {
      name: 'beer',
      enableBody: true
    }]
  });
  removeCurrentLevel = function() {
    if (levels[currentLevel].gameObjects) {
      levels[currentLevel].gameObjects.forEach(function(element) {
        element.destroy();
      });
    }
  };

  addNextLevel = function(game) {
    Hadrons.LevelManager.collideables = [];
    levels[currentLevel].gameObjects = [];

    levels[currentLevel].gameGroups = [];
    levels[currentLevel].rawGroups.forEach(function(group) {
      levels[currentLevel].gameGroups[group.name] = game.add.group();
      if (group.enableBody) {
        levels[currentLevel].gameGroups[group.name].enableBody = group.enableBody;
        Hadrons.LevelManager.collideables.push(levels[currentLevel].gameGroups[group.name]);
      }


    });

    levels[currentLevel].gameObjects = [];
    levels[currentLevel].rawObjects.forEach(function(object) {
      var newObject = levels[currentLevel].gameGroups[object.group].create(object.x, object.y, object.name);

      if (object.immovable) {
        newObject.body.immovable = true;
      }

      levels[currentLevel].gameObjects.push(newObject);
    });
  };

  self.loadLevel = function(params) {
    var levelNumber = params.levelNumber || 0,
      player = params.player,
      game = params.game;

    removeCurrentLevel();

    currentLevel = levelNumber;

    addNextLevel(game);


  };

  self.loadNextLevel = function(params) {
    if (currentLevel === levels.length - 1) {
      params.levelNumber = 0;
    } else {
      params.levelNumber = currentLevel + 1;
    }
    self.loadLevel(params);
    params.player.body.x = 0;
    return currentLevel;
  };

  self.loadPreviousLevel = function(params) {
    if (currentLevel === 0) {
      params.levelNumber = 0;
    } else {
      params.levelNumber = currentLevel - 1;
    }
    self.loadLevel(params);
    params.player.body.x = 750;
    return currentLevel;
  };

  Hadrons.LevelManager = self;
}());
