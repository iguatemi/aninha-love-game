/*global Phaser,Hadrons*/
(function () {
  'use strict';
  var game,
    playerWidth = 44,
    currentLevel = 0,
    player,
    cursors,
    fx,
    doWithInterval,
    playerWasJumping = true,
    preload = function () {
      game.load.spritesheet('player', 'images/player-00.png', playerWidth, 92);
      game.load.image('block', 'images/block.png');
      game.load.image('ground', 'images/ground.png');
      game.load.image('sky', 'images/sky.png');
      game.load.image('beer', 'images/beer.png');
      game.load.image('cerveja', 'images/cerveja.png');
      game.load.image('aninha', 'images/aninha.png');
      game.load.image('surpresa', 'images/surpresa.png');

      game.load.audio('sfx', ['sounds/sfx.mp3', 'sounds/sfx.ogg']);
    },
    create = function () {

      game.physics.startSystem(Phaser.Physics.ARCADE);

      game.add.sprite(0, 0, 'sky');

      player = game.add.sprite(60, 450, 'player');

      game.physics.arcade.enable(player);

      player.body.gravity.y = 3000;
      player.body.collideWorldBounds = false;

      player.animations.add('left', [1, 3], 7, true);
      player.animations.add('right', [1, 2], 7, true);

      cursors = game.input.keyboard.createCursorKeys();


      fx = game.add.audio('sfx');
      fx.allowMultiple = true;
      fx.addMarker('run', 1, 0.5);
      fx.addMarker('levelPassed', 2, 0.9);
      fx.addMarker('jump', 3, 0.9);
      fx.addMarker('land', 4, 0.9);

      Hadrons.LevelManager.loadLevel({
        levelNumber: currentLevel,
        game: game,
        player: player
      });
    },
    update = function () {

      Hadrons.LevelManager.collideables.forEach(function (collideable) {
        game.physics.arcade.collide(player, collideable);
      });

      player.body.velocity.x = 0;

      if (cursors.left.isDown) {
        if (player.body.touching.down) {
          player.animations.play('left');
          doWithInterval('playStep', 300, function () {
            fx.play('run');
          });
        } else {
          player.frame = 3;
        }
        player.body.velocity.x = -350;
      } else if (cursors.right.isDown) {
        if (player.body.touching.down) {
          player.animations.play('right');
          doWithInterval('playStep', 300, function () {
            fx.play('run');
          });
        } else {
          player.frame = 2;
        }
        player.body.velocity.x = 350;
      } else {
        player.animations.stop();
        player.frame = 0;
      }

      if (cursors.up.isDown && player.body.touching.down) {
        player.body.velocity.y = -1000;
        fx.play('jump');
        setTimeout(function () {
          playerWasJumping = true;
        }, 300);
      }
      
      if (player.body.touching.down) {
        if (playerWasJumping) {
          playerWasJumping = false;
          fx.play('land');
        }
      }
      
      if (player.body.x >= game.world.width - playerWidth) {
        currentLevel = Hadrons.LevelManager.loadNextLevel({
          game: game,
          player: player
        });
        fx.play('levelPassed');
      }

      if (player.body.x < 0) {
        currentLevel = Hadrons.LevelManager.loadPreviousLevel({
          game: game,
          player: player
        });
      }


    };

  doWithInterval = function (id, timeout, callback) {
    var intervals = window.intervals || {};

    if (!intervals[id]) {
      intervals[id] = Date.now();
    }
    
    if (intervals[id] + timeout <= Date.now()) {
      intervals[id] = false;
      callback();
    }
    
    window.intervals = intervals;
  };
  game = new Phaser.Game(800, 600, Phaser.CANVAS, '', {
    preload: preload,
    create: create,
    update: update
  });
}());